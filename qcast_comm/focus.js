//----------------------------------使用说明------------------------------------

//  ①载入顺序
//  focus.js需要创建两个div并且追加到body里面,建议作为body的第一个子元素载入
//
//  游戏中切换界面的js文件中需要调用focus.js中的方法，这些js应该放到focus.js后面载入
//  （总之，无论是直接还是间接调用focus.js中的方法，都放到其后面）
//
//  ②初始化配置
//  在模拟click或touch时，需要指定一个目标，游戏中应该都是canvas元素（可能是动态生成的），
//  建议在第一次初始化界面的时候设定，即第一次调用controlFocus.init()时调用controlFocus.setTarget()
//  因为第一个界面展示出来，就算canvas是动态生成，此刻也必定已生成好。
//

var tmp=0;
var flag;
var lastFocusList = {};
var focusConfig = {
    RectWidth: 1280,
    RectHeight: 720,
    RectLeft: 0,
    RectTop: 0,
    RectAspectRatio: 1.75,
    setAspectRatio: function(value) {
        this.RectAspectRatio = value;
    },
    init: function() {
        var w = window.innerWidth;
        var h = window.innerHeight;
        var windowAspectRatio = h/w;
        console.log("windowAspectRatio="+windowAspectRatio);
        if(windowAspectRatio >= this.RectAspectRatio){
            this.RectWidth = w;
            this.RectLeft = 0;
            this.RectHeight = this.RectWidth * this.RectAspectRatio;
            this.RectTop = (h - this.RectHeight)/2;
        }else{
            this.RectHeight = h;
            this.RectTop = 0;
            this.RectWidth = this.RectHeight / this.RectAspectRatio;
            this.RectLeft = (w- this.RectWidth)/2;
        }
    }
}


function remoterKeyDownCallback() {
    var ev = window.event;
    switch(ev.keyCode) {
        case 13: controlFocus.touchFocus();      break;
        case 4:  controlFocus.initBack();        break;
        case 37: controlFocus.moveLeft();        break;
        case 38: controlFocus.moveUp();          break;
        case 39: controlFocus.moveRight();       break;
        case 40: controlFocus.moveDown();        break;

        default:;
    }
}



//var Selector = {
//    create: function() {
//        var selector = {};
//        selector.change = arguments[0][0];
//        selector.url = arguments[0][1];
//        selector.hasAnotherImage = arguments[0][2];
//        selector.url2 = arguments[0][3];
//        selector.flag = false;
//
//        selector.div = document.createElement('div');
//        selector.div.style.position = "relative";
//        var tmp_info = focusImageConfig[selector.change - 1].size;
//
//        selector.div.style.width = tmp_info[0] * 0.95 + 'px';
//        selector.div.style.height = tmp_info[1] * 0.95  + 'px';
//        selector.div.style.backgroundImage = 'url(' + selector.url + ')';
//        selector.div.style.backgroundSize = '100% 100%';
//        selector.div.style.display = 'none';
//
//        selector.enable = function(b) {
//            b ? this.div.style.display = 'block' : this.div.style.display = 'none';
//        }
//
//        selector.addedToContainer = function(container) {
//            container.appendChild(this.div);
//        }
//
//        selector.removedFromContainer = function(container) {
//            container.removeChild(this.div);
//        }
//
//        selector.moveTo = function(theX, theY) {
//            this.div.style.left = theX - parseInt(this.div.style.width)/2 + 'px';
//            this.div.style.top = theY - parseInt(this.div.style.height)/2 + 'px';
//        }
//
//        selector.changeImage1 = function() {
//            flag=true;
//            this.div.style.backgroundImage = "url("+this.url2+")";
//        }
//        selector.changeImage2 = function() {
//            flag=false;
//            this.div.style.backgroundImage ="url("+this.url+")";
//        }
//
//
//        selector.onSelectorShifted = function(change, theX, theY) {
//            console.log(this.change);
//            if (change != this.change)
//            {
//                this.selector.enable(!1);
//            }
//            this.selector.moveTo(theX, theY);
//            this.selector.enable(!0);
//        }
//        return selector;
//    }
//}


var remoterFocus = {
    //"container": document.createElement("div"),
    //"o_body": document.getElementsByTagName('body')[0],
    //"currDiv" : {},
    //"selArray":[],

    "create": function() {
        //this.container.style.position = "absolute";
        //this.container.style.zIndex = 100;
        //this.container.style.overflow = "hidden";
        //this.container.style.top = "0px";
        //this.container.style.left = "0px";
        //this.container.style.width = window.innerWidth + "px";
        //this.container.style.height = window.innerHeight + "px";
        //this.container.style.display = "none";

        //for (var i = 0;i <= focusImageConfig.length-1;i++){
        //    var tmp_selector = Selector.create(focusImageConfig[i].assets);
        //    tmp_selector.addedToContainer(this.container);
        //    this.selArray.push(tmp_selector);
        //}

        //this.o_body.appendChild(this.container);
        //this.currDiv = this.getSelByChange(1);
        //
        //console.log("created the focus and container");
    },
    "getSelByChange": function(change) {
        //var match = {};
        //if(change == null){
        //    return this.selArray[0];
        //}
        //for (var i = 0; i < this.selArray.length; i++) {
        //    if (this.selArray[i].change == change) {
        //        match =this.selArray[i];
        //        break;
        //    }
        //}
        //return match;
    }
};

var controlFocus = {
    now_focus: {},
    focus_map: [],
    target: null,
    resetPosition: function() {
        remoterFocus.currDiv.enable(false);
        //var now = remoterFocus.getSelByChange(controlFocus.now_focus.needDrag);
        now.moveTo(this.now_focus.posLeft,this.now_focus.posTop);
        now.enable(true);
        remoterFocus.currDiv = now;
        lastFocusList[currentState[0]] = this.now_focus.focusIndex;
        console.log(lastFocusList)
    },
    setTarget: function(target) {
        this.target = target;
    },
    init: function(para_array) {
        if(typeof para_array != "undefined") {
            this.focus_map = [];
            for(var i = 0; i < para_array.length; i++){
                this.focus_map[i] = {};
                this.focus_map[i].posLeft = parseInt(focusConfig.RectWidth * para_array[i][0])
                                + focusConfig.RectLeft;
                this.focus_map[i].posTop = parseInt(focusConfig.RectHeight * para_array[i][1])
                                + focusConfig.RectTop;
                this.focus_map[i].left = para_array[i][2];
                this.focus_map[i].up = para_array[i][3];
                this.focus_map[i].right = para_array[i][4];
                this.focus_map[i].down = para_array[i][5];
                this.focus_map[i].focusIndex = i;
                this.focus_map[i].needDrag = para_array[i][7];

            }
            //window.addEventListener("keydown", remoterKeyDownCallback, false);
            ////remoterFocus.container.style.display = "block";
            //if(currentState[0].indexOf("CartMode") >= 0){
            //    switch(localStorage.frozen_v2_cart){
            //        case "0":
            //            this.now_focus = this.focus_map[5];
            //            break;
            //        case "1":
            //            this.now_focus = this.focus_map[8];
            //            break;
            //        case "2":
            //            this.now_focus = this.focus_map[7];
            //            break;
            //        case "3":
            //            this.now_focus = this.focus_map[9];
            //            break;
            //        default :
            //            this.now_focus = this.focus_map[0];
            //    }
            //} else {
            //    if(lastFocusList != null && lastFocusList[currentState[0]] != null){
            //  this.now_focus = this.focus_map[lastFocusList[currentState[0]]];
            //    } else {
                   this.now_focus = this.focus_map[0];
            //    }
            //}
           // this.resetPosition();
        }
        //else{
        //    console.log("para is undefined");
        //    window.removeEventListener("keydown", remoterKeyDownCallback, false);
        //    remoterFocus.container.style.display = "none";
        //}
    },
    touchFocus: function() {
        //trigTouchEvent();
         if(typeof jContentShellJBridge != "undefined" && jContentShellJBridge != null){
             trigMouseMoveDownUpEvent();
             //trigClickFromPoint();
            }else{
                //gjy
                if(currentState[0]!="MainMenu")
                    trigMouseMoveDownUpEvent();
                else
                    trigClickFromPoint();
            }

    },
    needMouseMove:function(){
        //if(controlFocus.now_focus.needDrag == 2 || controlFocus.now_focus.needDrag == 3){
            //triggerMouseOver();
       // }
    },
    initBack: function() {
        //will back to homepage;
    },
    moveLeft: function() {
        this.now_focus = this.focus_map[this.now_focus.left] || this.now_focus;
        this.resetPosition();
        triggerMouseOver();
    },
    moveUp: function() {
        this.now_focus = this.focus_map[this.now_focus.up] || this.now_focus;
        this.resetPosition();
        triggerMouseOver();
    },
    moveRight: function() {
        this.now_focus = this.focus_map[this.now_focus.right] || this.now_focus;
        this.resetPosition();
        triggerMouseOver();
    },
    moveDown: function() {
        this.now_focus = this.focus_map[this.now_focus.down] || this.now_focus;
        this.resetPosition();
        triggerMouseOver();
    },
    dragFocus: function(){
        tmp++;
        if(tmp%2==1){
            remoterFocus.currDiv.changeImage1();
            triggerMouseDown();
        }else{
            remoterFocus.currDiv.changeImage2();
            triggerMouseUp();
        }
    }

}

//trigger mouse event
function triggerMouseMove(){
    console.log("just mouse move");
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mousemove",true,true,document.defaultView,0,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);
}

function triggerMouseOver(){
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mouseover",true,true,document.defaultView,0,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);


}

function triggerMouseDown(){
    tmp=tmp%2;
    console.log("just mouse down");
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mousedown",true,true,document.defaultView,0,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);
}

function triggerMouseUp(){
    console.log("just mouse up");
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mouseup",true,true,document.defaultView,0,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);
}


function trigMouseMoveDownUpEvent(x,y) {
    console.log("trigMouseEvent")

    var tap_left,tap_top;
    if(x != null && y != null){
        tap_left = x * window.innerWidth;
        tap_top = y * window.innerHeight;
    } else {
        tap_left = controlFocus.now_focus.posLeft;
        tap_top = controlFocus.now_focus.posTop;
    }

    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mousemove",true,true,document.defaultView,0,
        tap_left,tap_top,tap_left,tap_top,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);

    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("mousedown",true,true,document.defaultView,0,
        tap_left,tap_top,tap_left,tap_top,
        false,false,false,false,0,null);
    controlFocus.target.dispatchEvent(evt);

    setTimeout(function() {
        var evt = document.createEvent("MouseEvents");
        evt.initMouseEvent("mouseup",true,true,document.defaultView,0,
            tap_left,tap_top,tap_left,tap_top,
            false,false,false,false,0,null);
        controlFocus.target.dispatchEvent(evt);
    },100);
}

var TouchId;
function trigTouchEvent(x,y) {
    console.log("trigMouseEvent")
    var tap_left,tap_top;
    if(x != null && y != null){
        tap_left = x * window.innerWidth;
        tap_top = y * window.innerHeight;
    } else {
        tap_left = controlFocus.now_focus.posLeft;
        tap_top = controlFocus.now_focus.posTop;
    }

    console.log("trigTouchEvent");
    TouchId = parseInt(Math.random() * 10000 + 1);
    var ts = document.createTouch(window, controlFocus.target, TouchId,
        tap_left,tap_top,tap_left,tap_top);
    var tls = document.createTouchList(ts);
    var evts = document.createEvent("TouchEvent");
    evts.initTouchEvent(tls, tls, tls, "touchstart", window, 0, 0, 0, 0, true, false, false, false);
    controlFocus.target.dispatchEvent(evts);

    setTimeout(function() {
        var te = document.createTouch(window, controlFocus.target, TouchId,
            tap_left,tap_top,tap_left,tap_top);
        var tle = document.createTouchList(te);
        var evte = document.createEvent("TouchEvent");
        evte.initTouchEvent(tle, tle, tle, "touchend", window, 0, 0, 0, 0, true, false, false, false);
        controlFocus.target.dispatchEvent(evte);
    },100);
}

function trigClickFromPoint(pram){

    remoterFocus.container.style.display = "none";
    var target = document.elementFromPoint(controlFocus.now_focus.posLeft, controlFocus.now_focus.posTop);
    if(target != null&& currentState[0] != "Play"){
        controlFocus.setTarget(target);
        trigMouseMoveDownUpEvent();
        //if(controlFocus.now_focus.needDrag == 1 || currentState[0] == "ReadyPage" || currentState[0] == "LevelFailed"){
        //    trigMouseMoveDownUpEvent();
        //}else{
        //    var evt = document.createEvent("MouseEvents");
        //    evt.initMouseEvent("click",true,true,document.defaultView,0,
        //        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        //        controlFocus.now_focus.posLeft,controlFocus.now_focus.posTop,
        //        false,false,false,false,0,null);
        //    target.dispatchEvent(evt);
        //}

    }

    setTimeout(function(){
            if((currentState[0] =="Pause" && controlFocus.now_focus == controlFocus.focus_map[3]) || currentState[0] == "Level"||currentState[0] == "Select" ||currentState[0] == "GameOver1"){
                remoterFocus.container.style.display = "block";
            }

        //remoterFocus.container.style.display = "block";
    },100);


}

//function alertMessage(msg,second){
//    if( typeof second == 'undefined')
//        second = 3000;
//    var toastDiv = document.createElement('div');
//    toastDiv.id = "toastDiv";
//    toastDiv.style.top = '80%';
//    toastDiv.style.left = '0';
//    toastDiv.style.padding = "0";
//    toastDiv.style.width = "1280px";
//    toastDiv.style.position = "fixed";
//    toastDiv.style.zIndex = "9999";
//    toastDiv.style.textAlign = "center";
//    toastDiv.style.display = "block";
//
//    var msgp= document.createElement('p');
//    msgp.style.backgroundColor = "rgba(0, 0, 0, 0.75)";
//    msgp.style.lineHeight = "35px";
//    msgp.style.fontSize = "28px";
//    msgp.style.color = "white";
//    msgp.style.padding = "10px";
//    msgp.style.display = "inline-block";
//    msgp.innerHTML = msg;
//
//    toastDiv.appendChild(msgp);
//    document.body.appendChild(toastDiv);
//    setTimeout(function(){
//        document.body.removeChild(document.getElementById("toastDiv"));
//        localStorage.cnt = 0;
//    },second);
//}