//qcast
var currentState = ["NotStart"];

//qcast comm setting
if (typeof jContentShellJBridge != "undefined" && jContentShellJBridge != null) {
    if (typeof jContentShellJBridge.indicateHomePageLoadDone != "undefined") {
        jContentShellJBridge.indicateHomePageLoadDone();
        console.log("-----indicateHomePageLoadDone-----");
    }

    jContentShellJBridge.disableMouseMode(true);

    // catch the system key event from java
    jContentShellJBridge.addJsCatchedKeyCode(4);    //back
    jContentShellJBridge.addJsCatchedKeyCode(82);   //menu

    document.grantAccessRight();
    document.assignNewUserAgent("Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36");

    //set the width of browser for fullscreen
    if (typeof jContentShellJBridge != "undefined") {
                jContentShellJBridge.disableMouseMode(true);
                var mvp = qcastTop.document.getElementById("viewport_handle");
                if (mvp != null) {
                    mvp.setAttribute("content", "width=1280");
                }
        console.log(">>>> setting the width to 1280 <<<<")
    }
}

//the callback function for java
function getNetworkTypeStatus(msg){
    console.log(msg)
}

//if(typeof jContentShellJBridge != "undefined"
//    && typeof jContentShellJBridge.getAppVersionString != "undefined"
//    && JSON.parse(jContentShellJBridge.getAppVersionString()).version_name.split(".")[2] == 184){
//    jContentShellJBridge.shutdownActivity();
//    isBackToHomepage = false;
//}
window.onkeydown = function(ev) {
    var keycode = ev.keyCode;
    console.log("onKeyDown, keycode =" + ev.keyCode);

    if(keycode == 4 || keycode == 27){
        console.log("catchdkey return");
        if(isAddCity){
            $(".buttomLayer").addClass("selectViewHide");
            $(".coverLayer").addClass("selectViewHide");
            $("#addBtn").css("background-image", "url(./images/add_normal.png)");
            $(cLi).removeClass("currSelect");
            $(tLi).removeClass("currSelect");
            isAddCity = false;
            document.removeEventListener("keydown",addCityWeatherControl,false);
            document.addEventListener("keydown", selectCityKeyDown, false);
        }else if(isEditCity){
            $(".coverLayer").addClass("selectViewHide");
            $(".editCityView").addClass("selectViewHide");
            $("#editUl").children("li").remove();
            $(".deleteBtn").removeClass("currSelect");
            $(".deleteBtn").removeClass("selecting");
            editCount=0;
            isEditCity =false;
            document.removeEventListener("keydown",editCityControl,false);
            document.addEventListener("keydown",selectCityKeyDown,false);
        } else {
            localStorage.cnt++;
            if (localStorage.cnt >= 2) {
                if (typeof jContentShellJBridge.backToHomepage == 'function') {
                    jContentShellJBridge.backToHomepage();
                }
                localStorage.cnt = 0;
            }
            var quitStr = "360天气";
            alertMessage("再按一次[返回]退出" + quitStr, 2000);
        }
    }
}

window.oncatchedkeyevent = function(keycode, keyaction) {
   var if_passthrough_key = false;
    if (keyaction == 0 && keycode == 82) {
        if(!isMainMenu) {
            console.log("zhouzm m is pressed");
            rootApp.detachResponders();
            rootApp.gui.switchToWindow("gamePause");
            //gjy
            isPause=true;
            pauseKeyControl();
        }
    }
    if(keycode == 4 && keyaction ==1 ){
        console.log("catchdkey return");
        if(isAddCity){
            $(".buttomLayer").addClass("selectViewHide");
            $(".coverLayer").addClass("selectViewHide");
            $("#addBtn").css("background-image", "url(./images/add_normal.png)");
            $(cLi).removeClass("currSelect");
            $(tLi).removeClass("currSelect");
            isAddCity = false;
            document.removeEventListener("keydown",addCityWeatherControl,false);
            document.addEventListener("keydown", selectCityKeyDown, false);
        }else if(isEditCity){
            $(".coverLayer").addClass("selectViewHide");
            $(".editCityView").addClass("selectViewHide");
            $("#editUl").children("li").remove();
            $(".deleteBtn").removeClass("currSelect");
            $(".deleteBtn").removeClass("selecting");
            editCount=0;
            isEditCity =false;
            document.removeEventListener("keydown",editCityControl,false);
            document.addEventListener("keydown",selectCityKeyDown,false);
        } else {
            if (!isBackToHomepage){
                if_passthrough_key = true;
            } else {
                localStorage.cnt++;
                if (localStorage.cnt >= 2) {
                    if (typeof jContentShellJBridge.backToHomepage == 'function') {
                        jContentShellJBridge.backToHomepage();
                    }
                    localStorage.cnt = 0;
                    if_passthrough_key = true;
                }
                var quitStr = "360天气";
                alertMessage("再按一次[返回]退出" + quitStr, 2000);
            }
        }
    }
    // Pass Thtough the Catched Key
    if (if_passthrough_key) {
        if (typeof jContentShellJBridge != "undefined") {
            jContentShellJBridge.passthroughCatchedKeyCode(keycode, keyaction);
        }
    }
}

//function reportGameState(pram){
//    if(currentState[0] != pram){
//        currentState.unshift(pram);
//    }
//
//    //if(pram == "beginCartMode"){
//    //    currentState.unshift("CartMode" + LocalStorage.get("cart"));
//    //}
//    initControlFocus();
//    console.log("currentState = ", currentState[0]);
//}
//
//function removeGameState(pram){
//    if(pram != null){
//        currentState.splice(currentState.indexOf(pram))
//    }else{
//        currentState.shift();
//    }
//    initControlFocus();
//    console.log("currentState = ", currentState[0]);
//}
//
//function initControlFocus(){
//    controlFocus.init(creditsFocusList[currentState[0]]);
//    controlFocus.setTarget(document.getElementsByTagName("canvas")[0]);
//    //controlFocus.setTarget(document.getElementById("rpjsLayer_DebugRendererSurface"));
//}
//
////add event listener
//window.addEventListener("load",
//    function() {
//        //controlFocus.setTarget(document.getElementById("c2canvas"));
//    },
//    false);
//remoterFocus.create();
function alertMessage(msg,second){
    if( typeof second == 'undefined')
        second = 3000;
    var toastDiv = document.createElement('div');
    toastDiv.id = "toastDiv";
    toastDiv.style.top = '80%';
    toastDiv.style.left = '0';
    toastDiv.style.padding = "0";
    toastDiv.style.width = "1280px";
    toastDiv.style.position = "fixed";
    toastDiv.style.zIndex = "9999";
    toastDiv.style.textAlign = "center";
    toastDiv.style.display = "block";

    var msgp= document.createElement('p');
    msgp.style.backgroundColor = "rgba(0, 0, 0, 0.75)";
    msgp.style.lineHeight = "35px";
    msgp.style.fontSize = "28px";
    msgp.style.color = "white";
    msgp.style.padding = "10px";
    msgp.style.display = "inline-block";
    msgp.innerHTML = msg;

    toastDiv.appendChild(msgp);
    document.body.appendChild(toastDiv);
    setTimeout(function(){
        document.body.removeChild(document.getElementById("toastDiv"));
        localStorage.cnt = 0;
    },second);
}